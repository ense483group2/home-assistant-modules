# The domain of your component. Should be equal to the name of your component.
DOMAIN = 'btmsg_service'

ATTR_NAME = 'name'
DEFAULT_NAME = 'default'


def setup(hass, config):
    """Set up is called when Home Assistant is loading our component."""

    def handle_btmsg(call):
        name = call.data.get(ATTR_NAME, DEFAULT_NAME)

        hass.states.set('btmsg_service.btmsg', name)

    hass.services.register(DOMAIN, 'btmsg', handle_btmsg)

    # Return boolean to indicate that initialization was successfully.
    return True

