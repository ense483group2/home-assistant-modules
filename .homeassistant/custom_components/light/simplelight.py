from homeassistant.components.light import Light
from homeassistant.const import CONF_HOST, STATE_ON, STATE_OFF
import requests

def setup_platform(hass, config, add_devices, discovery_info=None):
    host = config.get(CONF_HOST)
    add_devices([SimpleLight(host)])

class SimpleLight(Light):

    def __init__(self, host):
        self._name = "Simple Light"
        self._state = STATE_OFF
        self._host = host

    @property
    def name(self):
        """Return the display name of this light."""
        return self._name

    @property
    def is_on(self):
        return True if self._state == STATE_ON else False

    def turn_on(self, **kwargs):
        self._state = requests.post(self._host, json={"state": STATE_ON}).text
        return True

    def turn_off(self, **kwargs):
        self._state = requests.post(self._host, json={"state": STATE_OFF}).text
        return True

    def update(self):
        self._state = requests.get(self._host).text


