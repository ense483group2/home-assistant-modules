#!/usr/bin/env python

import time
from flask import Flask, request
app = Flask(__name__)

light_state = "off"

@app.route("/light", methods=["GET", "POST"])
def change_state():
	global light_state

	if request.method == "POST":
		light_state = request.get_json()['state']
		print "Changed state to", light_state
	
	return light_state

if __name__ == "__main__":
	app.run()
